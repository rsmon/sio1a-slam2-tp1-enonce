package programmes;

import gestionpersonnel.Salarie;

public class Exemple01 {

    public  void executer() {
      
        
        System.out.printf("\nListe de salariés à la date du: %-8s \n\n", utilitaires.UtilDate.aujourdhuiChaine());
      
        
        for(Salarie sal: gestionpersonnel.Entreprise.getTousLesSalaries()){
        
              System.out.printf(" %3d %-15s\n", sal.getId(),sal.getNom());
        
        }
        
        System.out.println();
    }
}

