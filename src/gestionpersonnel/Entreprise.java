
package gestionpersonnel;

import java.util.LinkedList;
import java.util.List;

public class Entreprise {
    
    //<editor-fold defaultstate="collapsed" desc="Attributs privés">
    
    private static List<Salarie>   tousLesSalaries= new LinkedList<Salarie>();
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Code Statique">
    
    static{
        
        Salarie s1=new Salarie();
        s1.setId(101L);
        s1.setNom("Durant");
        tousLesSalaries.add(s1);
        
        Salarie s2=new Salarie(102L,"Martin");
        tousLesSalaries.add(s2);
        
        tousLesSalaries.add( new Salarie(103L,"Lecoutre"));
        
    }
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Getters et Setters">
    
    public static List<Salarie> getTousLesSalaries() {
        return tousLesSalaries;
    }
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Méthodes Métiers">
    
    public static Salarie      getSalarieNumero(Long pId){
        
        Salarie salarie=null;
        
        for(Salarie unSalarie: tousLesSalaries){
            
            if( unSalarie.getId()==pId){ salarie=unSalarie;break;}
        }
        
        return salarie;
    }
    
    //</editor-fold>
}
