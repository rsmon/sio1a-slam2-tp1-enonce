
package gestionpersonnel;

public class Salarie {
    
    //<editor-fold defaultstate="collapsed" desc="Attributs">
    
    private Long id;
    
    private String nom;
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Constructeurs">
    
    public Salarie() { }
    
    public Salarie(Long id, String nom) {
        this.id = id;
        this.nom = nom;
    }
    
    //</editor-fold>
   
    //<editor-fold defaultstate="collapsed" desc="Getters et Setters">
    
    
    public Long getId() {
        return id;
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }
    
    
    public void setId(Long id) {
        this.id = id;
    }
    
    //</editor-fold>
    
    
}
